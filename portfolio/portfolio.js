// all jquery code will go inside this
$(document).ready(function() {

	// hide and show the hidden menu
	$('#hamburger-wrapper').click(function() {
		if($('.screen-size').width() < 850) {
			$('#menu').toggle();
		}
	});

	// toggling to hide for above, will not show menu
	// with media queries, idk why, so I have to
	// manually show and hide the menu
	$(window).resize(function() {
		if($('.screen-size').width() >= 850) {
			$('#menu').show();
		}
		else{
			$('#menu').hide();
		}
	});

	$('li').click(function() {
		if($('#hamburger-wrapper').css('display') == "none") {
			$('li').removeClass('highlight');
			$(this).addClass('highlight');
		}		
	});


}); // end of jquery