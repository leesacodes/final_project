# README #


### Capstone Project Plan ###

### Proposal ###
I am interested in creating a personal portfolio/resume page showcasing who I am, my skills, 
and projects to potential employers. I don't have much projects to show yet, but I've been wanting 
to create a professional portfolio template for the future. I wanted to use this capstone project 
opportunity to also demonstrate to potential employers my web development skills.


### Design and Goal ###
I strive to be a minimalist and simplist, and I would like to portray this life's philosophy towards 
my design: a simple easy to read web page. A light, fast, and responsive web page that is compatible 
with all screen sizes. I would like my visitor to see that the web page is clearly a portfolio. 
I want my visitors to be able to maneuver around my page frustration free in terms of understanding 
my contents and easily finding what they need.

### Tools ###
I did not use bootstrap. I wanted get more hands on practice with CSS by handcoding everything.
I was able to see how things worked under the hood.

I used jquery and css to create a drop down menu, when the screen sizes goes below 1040px.

I also used media query's to make the website responsive. I tested my website for responsiveness on my iphone 4s,
iphone 6s, ipad. I think screen sizes greater than 1400px, the website design gets a bit broken. 

### Inpiration ###
The following links are some websites that I find pleasing to the eyes in terms of design 
and/or special effects:

https://jpwdesignstudio.com/
https://dribbble.com/shots/2294838-Page-404
https://www.shutterstock.com/blog/10-gorgeous-color-schemes-for-websites
